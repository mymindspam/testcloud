from django.core.files.storage import FileSystemStorage
import hashlib


class DeduplicateFileSystemStorage(FileSystemStorage):

    def get_available_name(self, name):
        return name

    def _save(self, name, content):
        return super(DeduplicateFileSystemStorage, self)._save(name, content)

    @classmethod
    def checksum(cls, file):
        m = hashlib.md5()
        m.update(file.read())
        return m.hexdigest()