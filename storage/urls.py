from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'storage.views.file_list', name='file-list'),
    url(r'^accounts/profile/$', 'storage.views.file_list', name='file-list'),
    url(r'^download/(?P<user_id>\w+)/(?P<checksum>\w+)/$', 'storage.views.download_file', name='download-file'),
    url(r'^delete/(?P<pk>\d+)/$', 'storage.views.delete_file', name='delete-file'),
)
