from django.contrib.auth.models import User
from django.db import models
from storage import DeduplicateFileSystemStorage


class Media(models.Model):
    upload = models.FileField(storage=DeduplicateFileSystemStorage())
    checksum = models.CharField(max_length=32, primary_key=True)


class File(models.Model):
    user = models.ForeignKey(User)
    media = models.ForeignKey(Media)
    name = models.CharField(max_length=140)
