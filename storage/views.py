from django.http.response import HttpResponse
from django.shortcuts import render, redirect, get_list_or_404, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.template.context import RequestContext

from models import File
from models import Media
from django.contrib.auth.models import User
from forms import FileForm
from storage import DeduplicateFileSystemStorage


@login_required
def file_list(request):
    args = {'user': request.user}
    is_limit_expired = File.objects.filter(user=request.user).count() == 100
    if request.method == 'POST' and not is_limit_expired:
        form = FileForm(request.POST, request.FILES)
        if form.is_valid():
            upload = request.FILES['file']
            checksum = DeduplicateFileSystemStorage.checksum(upload)

            try:
                media = Media.objects.get(checksum=checksum)
                args['share_messages'] = ['uploaded by ' + file.user.username + ' as ' + file.name
                                          for file in File.objects.filter(media=media)]
            except:
                media = Media(upload=upload, checksum=checksum)
                media.save()
            newfile = File(user=request.user, name=upload.name, media=media)
            newfile.save()
    if is_limit_expired:
        args['warning'] = 'Files limit expired'
    else:
        args['form'] = FileForm()
    args['items'] = [{'file': file, 'link': file.media.checksum}
                     for file in File.objects.filter(user=request.user)]
    return render(request, 'profile.html', args, context_instance=RequestContext(request))

@login_required
def delete_file(request, pk):
    file = get_object_or_404(File, pk=pk)
    files = get_list_or_404(File, media=file.media)
    if len(files) == 1:
        file.media.upload.delete()
        file.media.delete()
    file.delete()
    return redirect('/')


def download_file(request, user_id, checksum):
    user = get_object_or_404(User, pk=user_id)
    media = Media.objects.get(checksum=checksum)
    files = File.objects.filter(media=media, user=user)
    response = HttpResponse(media.upload.read())
    response['Content-Disposition'] = 'attachment; filename=%s' % files[0].name
    return response
